setwd("C:/Users/yardensasunmu/Downloads")

data.raw <-read.csv("data.csv")

data<-data.raw


str(data$evaluation)
summary(data$evaluation)
summary(is.na.data.frame(data))



#Q1

prb <- as.integer(data$evaluation)
swap <- function(x)
{
  if (x==1)
    return(2)
  else return (x)
}

swap2 <- function(x)
{
  if (x==4)
    return(2)
  else return (x)
}



prb <- sapply(prb, swap)
prb <- sapply(prb, swap2)

str(prb)
prb<-factor(prb, labels = c("good", "unacc"))
summary(prb)
data$evaluation<-prb
summary(data$evaluation)




#Q2
library(ggplot2)

#�
summary(data)
ggplot(data , aes(buying.price, fill = evaluation)) + geom_bar()


#�
ggplot(data) + aes(x = buying.price, fill = maint.price) + geom_bar(position = "fill") 

ggplot(data, aes(buying.price)) + geom_bar(aes(fill = maint.price))

pl4 <- ggplot(data, aes(x = maint.price))
pl4 <- pl4 + geom_bar(color = 'blue', fill = 'light blue')
pl4 <- pl4 + geom_bar(aes(fill = buying.price))

pl4 <- pl4 + geom_bar(aes(fill = buying.price), position = 'fill')



#�
test <-as.integer(data$doors)
ggplot(data,aes(evaluation,test)) + geom_boxplot()
ggplot(data , aes(doors, fill = evaluation)) + geom_bar()



#�
ggplot(data) + aes(x = safty, fill = evaluation) + geom_bar(position = "fill") 



#Q3
library(caTools)
filter <- sample.split(data$persons, SplitRatio = 0.7)
car.train <- subset(data, filter == T)
car.test <- subset(data, filter == F)
dim(data)
dim(car.train)
dim(car.test)


library(rpart)
install.packages("rpart.plot")
library(rpart.plot)

model1<- rpart(evaluation~safty+doors,data)
#���� �� ��� ������
rpart.plot(model1 , box.palette = 'RdBu', shadow.col = 'grey', nn = TRUE)

prediction.prob1 <- predict(model1, car.test)

prediction.prob1 <- as.data.frame(prediction.prob1)

prediction1 <- prediction.prob1$good> 0.5

actual <- car.test$evaluation



confusion1 <- table(prediction1 ,actual )

class(confusion1)

TN1 <- confusion1[1,1]
FN1 <- confusion1[1,2]
FP1 <- confusion1[2,1]
TP1 <- confusion1[2,2]

recall1 <- TP1/(TP1+FN1)
precision1 <- TP1/(TP1+FP1)

recall1
precision1



model2<- rpart(evaluation~ buying.price+persons,data)
#���� �� ��� ������
rpart.plot(model2 , box.palette = 'RdBu', shadow.col = 'grey', nn = TRUE)

prediction.prob2 <- predict(model2, car.test)

prediction.prob2 <- as.data.frame(prediction.prob2)

prediction2 <- prediction.prob2$good> 0.5

actual <- car.test$evaluation



confusion2 <- table(prediction2 ,actual )


TN2 <- confusion2[1,1]
FN2 <- confusion2[1,2]
FP2 <- confusion2[2,1]
TP2 <- confusion2[2,2]

recall2 <- TP2/(TP2+FN2)
precision2 <- TP2/(TP2+FP2)

recall2
precision2



#�

